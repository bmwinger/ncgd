# Natural Character Growth and Decay

This is a demonstration of the proof of concept software [EspMarkup](https://gitlab.com/bmwinger/espmarkup), showing a version-control friendly way of storing elder scrolls plugin information.

All credit for the NCGD goes to its upstream authors:
- https://www.nexusmods.com/morrowind/mods/44967
- https://modding-openmw.com/mods/natural-character-growth-and-decay-mw/
